import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ���� ��������-������ �� ����� 3x3
 *
 * Alterovych Ilya 2014
 */
public class TicTacToe extends Applet implements ActionListener {
    private static final String HUMAN = "X";
    private static final String COMPUTER = "O";
    Button[][] squares;
    Button newGameButton;
    Label score;
    Label compWin;
    Label playWin;
    int cWin;
    int pWin;
    int emptySquaresLeft = 9;

    public static void main(String[] args) {
        TicTacToe tic = new TicTacToe();
        tic.init();
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setContentPane(tic);
        f.setMinimumSize(new Dimension(500, 500));
        f.setVisible(true);
    }

    /**
     * ����� init � ��� ����������� �������
     */
    public void init() {
        //������������� �������� ������������ �������, ������, ����� � ����
        this.setLayout(new BorderLayout());
        this.setSize(500, 500);
        this.setBackground(Color.LIGHT_GRAY);

        // �������� ����� ������� ���, ����� �� ��� ������
        // � ���� ������ 20
        Font appletFont = new Font("Monospaced", Font.BOLD, 20);
        this.setFont(appletFont);

        // ������� ������ �New Game� � ������������ � ���
        // ��������� ��������
        newGameButton = new Button("����� ����");
        newGameButton.addActionListener(this);
        compWin = new Label("���� �������: " + cWin);
        compWin.setForeground(Color.RED);
        playWin = new Label(" �� �������: " + pWin);
        playWin.setForeground(Color.BLUE);

        Panel topPanel = new Panel();
        topPanel.setLayout(new BorderLayout());
        topPanel.add(newGameButton, "North");
        topPanel.add(compWin, "West");
        topPanel.add(playWin, "East");
        this.add(topPanel, "North");

        Panel centerPanel = new Panel();
        centerPanel.setLayout(new GridLayout(3, 3, 5, 5));
        this.add(centerPanel, "Center");
        score = new Label("���� ���!");

        Panel bottomPanel = new Panel();
        bottomPanel.add(score);
        this.add(bottomPanel, "South");

        // ������� ������, ����� ������� ������ �� 9 ������
        squares = new Button[3][3];

        // ������� ������, ��������� ������ �� ��� � �������
        // ������������ � ��� ���������, ������ ��
        // � ����� ���� � ��������� �� ������
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                squares[i][j] = new Button();
                squares[i][j].addActionListener(this);
                squares[i][j].setBackground(Color.YELLOW);
                centerPanel.add(squares[i][j]);
            }
        }
    }

    /**
     * ���� ����� ����� ������������ ��� �������
     *
     * @param e ActionEvent ������
     */
    public void actionPerformed(ActionEvent e) {
        Button theButton = (Button) e.getSource();
        // ��� ������ New Game ?
        if (theButton == newGameButton) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    squares[i][j].setEnabled(true);
                    squares[i][j].setLabel("");
                    squares[i][j].setBackground(Color.YELLOW);
                }
            }
            emptySquaresLeft = 9;
            score.setForeground(Color.BLACK);
            score.setText("���� ���!");
            newGameButton.setEnabled(true);
            return;
        }

        String winner = "";
        // ��� ���� �� ������?
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (theButton == squares[i][j] && squares[i][j].getLabel().equals("")) {
                    squares[i][j].setLabel(HUMAN);
                    winner = lookForWinner();
                    if (!"".equals(winner)) {
                        endTheGame();
                    } else {
                        computerMove();
                        winner = lookForWinner();
                        if (!"".equals(winner)) {
                            endTheGame();
                        }
                    }
                    break;
                }
            }
        }

        switch (winner) {
            case HUMAN:
                pWin++;
                playWin.setText(" �� �������: " + pWin);
                score.setForeground(Color.BLUE);
                score.setText("�� �������!");
                break;
            case COMPUTER:
                cWin++;
                compWin.setText("���� �������: " + cWin);
                score.setForeground(Color.RED);
                score.setText("�� ������!");
                break;
            case "T":
                score.setText("�����!");
                break;
        }
    }

    /**
     * ���� ����� ���������� ����� ������� ����, ����� ������,
     * ���� �� ����������. �� ��������� ������ ���, �������
     * � ���������, ����� ����� ��� ������ � ����������� ���������
     * (�� �������)
     *
     * @return "X", "O", "T" � �����, "" - ��� ��� ����������
     */
    String lookForWinner() {
        String theWinner = "";
        emptySquaresLeft--;
        if (emptySquaresLeft == 0) {
            return "T"; // ��� �����. T �� ����������� ����� tie
        }

        // ��������� ��� 1 � �������� ������� 0,1,2
        if (!squares[0][0].getLabel().equals("") &&
                squares[0][0].getLabel().equals(squares[0][1].getLabel()) &&
                squares[0][0].getLabel().equals(squares[0][2].getLabel())) {
            theWinner = squares[0][0].getLabel();
            highlightWinner(0, 0, 0, 1, 0, 2);
            // ��������� ��� 2 � �������� ������� 3,4,5
        } else if (!squares[1][0].getLabel().equals("") &&
                squares[1][0].getLabel().equals(squares[1][1].getLabel()) &&
                squares[1][0].getLabel().equals(squares[1][2].getLabel())) {
            theWinner = squares[1][0].getLabel();
            highlightWinner(1, 0, 1, 1, 1, 2);
            // ��������� ��� 3 � �������� ������� 6,7,8
        } else if (!squares[2][0].getLabel().equals("") &&
                squares[2][0].getLabel().equals(squares[2][1].getLabel()) &&
                squares[2][0].getLabel().equals(squares[2][2].getLabel())) {
            theWinner = squares[2][0].getLabel();
            highlightWinner(2, 0, 2, 1, 2, 2);
            // ��������� ������� 1 � �������� ������� 0,3,6
        } else if (!squares[0][0].getLabel().equals("") &&
                squares[0][0].getLabel().equals(squares[1][0].getLabel()) &&
                squares[0][0].getLabel().equals(squares[2][0].getLabel())) {
            theWinner = squares[0][0].getLabel();
            highlightWinner(0, 0, 1, 0, 2, 0);
            // ��������� ������� 2 � �������� ������� 1,4,7
        } else if (!squares[0][1].getLabel().equals("") &&
                squares[0][1].getLabel().equals(squares[1][1].getLabel()) &&
                squares[0][1].getLabel().equals(squares[2][1].getLabel())) {
            theWinner = squares[0][1].getLabel();
            highlightWinner(0, 1, 1, 1, 2, 1);
            // ��������� ������� 3 � �������� ������� 2,5,8
        } else if (!squares[0][2].getLabel().equals("") &&
                squares[0][2].getLabel().equals(squares[1][2].getLabel()) &&
                squares[0][2].getLabel().equals(squares[2][2].getLabel())) {
            theWinner = squares[0][2].getLabel();
            highlightWinner(0, 2, 1, 2, 2, 2);
            // ��������� ������ ��������� � �������� ������� 0,4,8
        } else if (!squares[0][0].getLabel().equals("") &&
                squares[0][0].getLabel().equals(squares[1][1].getLabel()) &&
                squares[0][0].getLabel().equals(squares[2][2].getLabel())) {
            theWinner = squares[0][0].getLabel();
            highlightWinner(0, 0, 1, 1, 2, 2);
            // ��������� ������ ��������� � �������� ������� 2,4,6
        } else if (!squares[0][2].getLabel().equals("") &&
                squares[0][2].getLabel().equals(squares[1][1].getLabel()) &&
                squares[0][2].getLabel().equals(squares[2][0].getLabel())) {
            theWinner = squares[0][2].getLabel();
            highlightWinner(0, 2, 1, 1, 2, 0);
        }
        return theWinner;
    }

    /**
     * ���� ����� ��������� ����� ������, ����� �����
     * ������ ������������ ���. ���� ������� ���
     * �� ������, ���������� ��������� ������.
     */
    void computerMove() {
        int selectedSquare;
        int i;
        int j;

        // ������� ��������� �������� ����� ������ ������
        // ����� � ����� �������� � ��������, ����� ��������
        selectedSquare = findLastSquare(COMPUTER);

        // ���� �� �� ����� ����� ��� ������, �� ���� ��
        // ���������� �� ���� ��������� ������� ��� �� 3-�
        // ���������,�������� ����� ����� � ����� ����������
        if (selectedSquare == -1)
            selectedSquare = findLastSquare(HUMAN);

        // ���� selectedSquare ��� ��� ����� -1, ��
        // ���������� ������ ����������� ������
        if ((selectedSquare == -1) && (squares[1][1].getLabel().equals(""))) {
            selectedSquare = 4;
        }

        // �� ������� � ����������� �������...
        // ��������� ���� �� �� ������� ����
        if (selectedSquare == -1) {
            selectedSquare = getRandomAngle();
        }

        // ������ �������� ��������� ������
        if (selectedSquare == -1) {
            selectedSquare = getRandomSquare();
        }

        //����������� ����� ��������� ������ selectedSquare
        //� � ���������� � ������� squares[i][j] � ������� ������ SquareToArray
        int[] array = squareToCoordinates(selectedSquare);
        i = array[0];
        j = array[1];
        squares[i][j].setLabel(COMPUTER);
    }

    /**
     * ���� ����� ��������� ������ ���, ������� � ���������
     * ����� ������, ���� �� � ��� ��� ������
     * � ����������� ��������� � ������ �������.
     *
     * @param player ���������� X � ��� ������������ � O � ��� �����
     * @return ���������� ��������� ������,
     * ��� -1, ���� �� ������� ��� ������
     * � ����������� ���������
     */
    int findLastSquare(String player) {
        int[][] weight = new int[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (squares[i][j].getLabel().equals(COMPUTER))
                    weight[i][j] = -1;
                else if (squares[i][j].getLabel().equals(HUMAN))
                    weight[i][j] = 1;
                else
                    weight[i][j] = 0;
            }
        }
        int twoWeights = player.equals(COMPUTER) ? -2 : 2;

        // ��������, ���� �� � ���� 1 ��� ���������� ������ �
        // ���� ������.
        if (weight[0][0] + weight[0][1] + weight[0][2] == twoWeights) {
            if (weight[0][0] == 0)
                return 0;
            else if (weight[0][1] == 0)
                return 1;
            else
                return 2;
        }

        // ��������, ���� �� � ���� 2 ��� ���������� ������ �
        // ���� ������.
        if (weight[1][0] + weight[1][1] + weight[1][2] == twoWeights) {
            if (weight[1][0] == 0)
                return 3;
            else if (weight[1][1] == 0)
                return 4;
            else
                return 5;
        }

        // ��������, ���� �� � ���� 3 ��� ���������� ������ �
        // ���� ������.
        if (weight[2][0] + weight[2][1] + weight[2][2] == twoWeights) {
            if (weight[2][0] == 0)
                return 6;
            else if (weight[2][1] == 0)
                return 7;
            else
                return 8;
        }

        // ��������, ���� �� � ������� 1 ��� ���������� ������ �
        // ���� ������.
        if (weight[0][0] + weight[1][0] + weight[2][0] == twoWeights) {
            if (weight[0][0] == 0)
                return 0;
            else if (weight[1][0] == 0)
                return 3;
            else
                return 6;
        }

        // ��������, ���� �� � ������� 2 ��� ���������� ������
        // � ���� ������.
        if (weight[0][1] + weight[1][1] + weight[2][1] == twoWeights) {
            if (weight[0][1] == 0)
                return 1;
            else if (weight[1][1] == 0)
                return 4;
            else
                return 7;
        }

        // ��������, ���� �� � ������� 3 ��� ���������� ������
        // � ���� ������.
        if (weight[0][2] + weight[1][2] + weight[2][2] == twoWeights) {
            if (weight[0][2] == 0)
                return 2;
            else if (weight[1][2] == 0)
                return 5;
            else
                return 8;
        }

        // ��������, ���� �� � ��������� 1 ��� ���������� ������
        // � ���� ������.
        if (weight[0][0] + weight[1][1] + weight[2][2] == twoWeights) {
            if (weight[0][0] == 0)
                return 0;
            else if (weight[1][1] == 0)
                return 4;
            else
                return 8;
        }

        // ��������, ���� �� � ��������� 2 ��� ���������� ������
        // � ���� ������.
        if (weight[0][2] + weight[1][1] + weight[2][0] == twoWeights) {
            if (weight[0][2] == 0)
                return 2;
            else if (weight[1][1] == 0)
                return 4;
            else
                return 6;
        }
        // �� ������� ���� ���������� �������� ������
        return -1;
    }

    /**
     * ���� ����� �������� ��������� ��������� ������� ������
     *
     * @return �������� ��������� ����� ��������� ������� ������
     */
    int getRandomAngle() {
        if (!squares[0][0].getLabel().equals("")
                && !squares[0][2].getLabel().equals("")
                && !squares[2][0].getLabel().equals("")
                && !squares[2][2].getLabel().equals("")
        ) {
            return -1;
        }

        do {
            int randomAngle;

            do {
                randomAngle = (int) (Math.random() * 9);
            } while (randomAngle != 0 && randomAngle != 2 && randomAngle != 6 && randomAngle != 8);

            int[] coordinates = squareToCoordinates(randomAngle);
            int i = coordinates[0];
            int j = coordinates[1];

            if (squares[i][j].getLabel().equals("")) {
                return randomAngle;
            }
        } while (true);
    }

    /**
     * ���� ����� �������� ����� ������ ������
     *
     * @return �������� ��������� ����� ������
     */
    int getRandomSquare() {
        int selectedSquare;
        int i;
        int j;
        do {
            selectedSquare = (int) (Math.random() * 9);
            //����������� ����� ��������� ������ selectedSquare
            //� � ���������� � ������� squares[i][j] � ������� ������ SquareToArray
            int[] array = squareToCoordinates(selectedSquare);
            i = array[0];
            j = array[1];
        } while (!squares[i][j].getLabel().equals(""));
        return selectedSquare;
    }

    /**
     * ���� ����� �������� ���������� �����.
     * ������, ������ � ������ ������ ��� ���������
     */
    void highlightWinner(int win1, int win11, int win2, int win22, int win3, int win33) {
        if (squares[win1][win11].getLabel().equals(COMPUTER)) {
            squares[win1][win11].setBackground(Color.RED);
            squares[win2][win22].setBackground(Color.RED);
            squares[win3][win33].setBackground(Color.RED);
        } else {
            squares[win1][win11].setBackground(Color.BLUE);
            squares[win2][win22].setBackground(Color.BLUE);
            squares[win3][win33].setBackground(Color.BLUE);
        }

    }

    // ������ ������������ ������
    void endTheGame() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                squares[i][j].setEnabled(false);
            }
        }
    }

    int[] squareToCoordinates(int selectedSquare) {
        int i = 0;
        int j = 0;
        switch (selectedSquare) {
            case 0:
                i = 0;
                j = 0;
                break;
            case 1:
                i = 0;
                j = 1;
                break;
            case 2:
                i = 0;
                j = 2;
                break;
            case 3:
                i = 1;
                j = 0;
                break;
            case 4:
                i = 1;
                j = 1;
                break;
            case 5:
                i = 1;
                j = 2;
                break;
            case 6:
                i = 2;
                j = 0;
                break;
            case 7:
                i = 2;
                j = 1;
                break;
            case 8:
                i = 2;
                j = 2;
                break;
        }

        return new int[]{i, j};
    }

}
